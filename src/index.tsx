import React, {Component, useState} from 'react';
import {StyleSheet, TouchableWithoutFeedback, Dimensions, Text} from 'react-native';

import Animated from 'react-native-reanimated';

const {
  divide,
  set,
  cond,
  startClock,
  stopClock,
  clockRunning,
  block,
  spring,
  debug,
  Value,
  Clock,
  SpringUtils,
  event,
  eq
} = Animated;

const config = SpringUtils.makeConfigFromBouncinessAndSpeed({
  toValue: new Value(0),
  damping: 10,
  mass: 5,
  stiffness: 101.6,
  overshootClamping: true,
  bounciness: -15,
  speed: 10
});

function runSpring(clock, value, dest, config) {
  const state = {
    finished: new Value(0),
    velocity: new Value(0),
    position: new Value(0),
    // time: new Value(0),
  };

  return block([
    cond(clockRunning(clock), 0, [
      set(state.finished, 0),
      // set(state.time, 1),
      set(state.position, value),
      set(config.toValue, dest),
      startClock(clock),
    ]),
    spring(clock, state, config),
    cond(state.finished, debug('stop clock', stopClock(clock))),
    state.position,
  ]);
}

type Props = {
  buttonSize: number,
  extendWidth: number,
  animationConfig: any,
  backgroundColor: string,
  textContent: string
};

export default function ButtonTextInput(
  {
    buttonSize = 100,
    extendWidth = Math.round(Dimensions.get('window').width) - 32,
    animationConfig = config,
    backgroundColor = 'green',
    textContent = 'Others'
  }) {
  const [isOpen, setIsOpen] = useState(false);
  let _trans = buttonSize;
  if (isOpen) {
    const clock = new Clock();
    _trans = runSpring(clock, buttonSize, extendWidth, animationConfig);
  }
  return (
    <TouchableWithoutFeedback onPress={() => {
      setIsOpen(!isOpen)
    }}>
      <Animated.View
        style={[
          styles.box,
          {
            width: _trans,
            height: buttonSize,
            backgroundColor
          },
        ]}>
        <Text style={{color: 'white', fontWeight: 'bold', opacity: isOpen ? 0 : 1}}>{textContent}</Text>
      </Animated.View>
    </TouchableWithoutFeedback>
  );
}

const BOX_SIZE = 100;

const styles = StyleSheet.create({
  text: {
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white',
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#fb628c',
    backgroundColor: '#2e13ff',
  },
  box: {
    width: 50,
    height: BOX_SIZE,
    borderColor: '#f900ff',
    backgroundColor: '#19ff75',
    margin: 10,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
});
